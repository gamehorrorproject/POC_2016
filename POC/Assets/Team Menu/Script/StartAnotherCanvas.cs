﻿using UnityEngine;
using System.Collections;

public class StartAnotherCanvas : MonoBehaviour {

	public GameObject CurentCanvas;
	public GameObject MainCanvas;

	public void LoadMainCanvas()
	{
		MainCanvas.SetActive (true);
		CurentCanvas.SetActive (false);
	}
}
