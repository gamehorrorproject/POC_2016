﻿using UnityEngine;
using System.Collections;

public class StartSettingsCanvas : MonoBehaviour {

	public GameObject SettingsCanvas;
	public GameObject Canvas;

	public void LoadCanvasSettings()
	{
		Canvas.SetActive (false);
		SettingsCanvas.SetActive (true);
	}
}
