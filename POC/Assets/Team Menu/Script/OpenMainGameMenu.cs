﻿using UnityEngine;
using System.Collections;


/*sert a ouvrir et a fermer les menu du jeu */
public class OpenMainGameMenu : MonoBehaviour {

	public GameObject MainMenuCanvas;
	public GameObject InGameSettingsMenu;

	public void OpenMainMenu(int toActive)
	{
		if (toActive == 1)
			MainMenuCanvas.SetActive (true);
		else if (toActive == 2)
			InGameSettingsMenu.SetActive (true);
		else if (toActive == 3)
			InGameSettingsMenu.SetActive (false);
		else 
			MainMenuCanvas.SetActive (false);
	}
}
