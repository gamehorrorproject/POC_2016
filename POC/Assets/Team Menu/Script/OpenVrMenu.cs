﻿using UnityEngine;
using System.Collections;

public class OpenVrMenu : MonoBehaviour {

	public GameObject SettingsCanvas;

	public void OpenVRMenu(int toActive)
	{
		if (toActive == 1)
			SettingsCanvas.SetActive (true);
		else 
			SettingsCanvas.SetActive (false);
	}
}
