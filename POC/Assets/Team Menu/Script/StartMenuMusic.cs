﻿using UnityEngine;
using System.Collections;

public class StartMenuMusic : MonoBehaviour {

	public AudioSource music;
	// Use this for initialization
	void Start () {
		music.Play ();
		PlayerPrefs.SetString ("Music", "YES");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
