﻿using UnityEngine;
using System.Collections;

public class SongSettingsInGame : MonoBehaviour {

	public AudioSource Music;

	public void StopStartSongFromInGameMenu()
	{
		if (Music.isPlaying)
			Music.Stop ();
		else
			Music.Play();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
