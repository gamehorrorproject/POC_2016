﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadingBar : MonoBehaviour {

	public Slider loadingBar;
	public GameObject LoadingImage;


	private AsyncOperation async;


	public void ClickAsync(int level)
	{
		LoadingImage.SetActive(true);
		StartCoroutine(LoadLevelWithBar(level));
	}


	IEnumerator LoadLevelWithBar (int level)
	{
		async = Application.LoadLevelAsync(level);
		while (!async.isDone)
		{
			loadingBar.value = async.progress;
			yield return null;
		}
	}
}
