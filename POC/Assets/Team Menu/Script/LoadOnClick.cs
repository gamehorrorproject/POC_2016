﻿using UnityEngine;
using System.Collections;

public class LoadOnClick : MonoBehaviour {

	public GameObject LoadingImage;

	public void LoadScene(int level, AudioSource au)
	{
		LoadingImage.SetActive(true);

		Application.LoadLevel(level);
	}
}