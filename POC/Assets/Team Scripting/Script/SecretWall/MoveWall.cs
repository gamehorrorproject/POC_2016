﻿using UnityEngine;
using System.Collections;

public class MoveWall : MonoBehaviour {

	public float smooth = 2.0f;
	public float depth = 10.0f;
	float limit = -10;
	bool isActive = false;
	bool move;


	void start()
	{
		limit = 0 - depth;
	}

	//Main function
	void Update ( )
	{
		Vector3 target = new Vector3 (transform.position.x, (transform.position.y - depth), transform.position.z);

		if (move && transform.position.y > limit) {
			transform.position = Vector3.Lerp (transform.position, target, Time.deltaTime * smooth);
			isActive = true;
		}
	}

	public void Move()
	{
		Debug.Log ("Interaction avec le mur");
		limit = 0 - depth;
		bool tmp = !move;
		move = tmp;
	}

	public bool IsActive
	{
		get {return (isActive);}
	}
}
