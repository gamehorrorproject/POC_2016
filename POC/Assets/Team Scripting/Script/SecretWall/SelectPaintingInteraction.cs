﻿using UnityEngine;
using System.Collections;

public class SelectPaintingInteraction : MonoBehaviour {

	bool startLookingAt = false;
	bool stopLookingAt = false;
	float duration;
	public float seconds = 2;
	public GameObject wall;
	public CardboardHead head;
	public float maxRange;
    public AudioClip _sound;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (startLookingAt && !stopLookingAt) {
			duration += Time.deltaTime;
		}

		if (duration >= seconds) {
            if (!wall.GetComponent<MoveWall>().IsActive)
            {
                GetComponent<AudioSource>().PlayOneShot(_sound, 0.1f);
                wall.GetComponent<MoveWall>().Move();
            }
		}
	}

	public void OnLookAt()
	{
		RaycastHit hit;
		Collider collider = GetComponent<Collider>();
		bool isLookedAt = collider.Raycast (head.Gaze, out hit, maxRange);
        GameObject FlashLight = GameObject.Find("CardboardMain/Head/VrCanvas/FlashLightInventory");
		if (isLookedAt && FlashLight.activeInHierarchy == true)
		{
			startLookingAt = true;
			stopLookingAt = false;
			duration = 0.0f;
		}
	}

	public void ExitLookAt()
	{
		startLookingAt = false;
		stopLookingAt = true;
		duration = 0.0f;
	}
}
