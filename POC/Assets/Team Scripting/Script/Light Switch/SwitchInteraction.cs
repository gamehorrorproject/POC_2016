﻿using UnityEngine;
using System.Collections;

public class SwitchInteraction : MonoBehaviour
{
	public float smooth = 10.0f;
	public float SwitchOn = -0.005f;
	public float SwitchOff = 0.005f;
	public bool poweron = false;
	public bool click = false;


	//Main function
	void Update ( )
	{
		if (click == true) {
			if (poweron == true) { 
				transform.Translate (0, SwitchOn, 0);
				click = false;
			}
			else if (poweron == false) {
				transform.Translate (0, SwitchOff, 0);
				click = false;
			}
		}
	}

	public void InteractWithSwitch()
	{
		bool tmp = !poweron;
		click = true;
		poweron = tmp;
	}
}
//0.2294292         -0.1510215           0.1830294