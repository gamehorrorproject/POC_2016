﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndLevel : IActionScript {

    public Slider _loadingBar;
    private AsyncOperation async;
    public int _level;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void Work()
    {
        async = Application.LoadLevelAsync(_level);
        while (!async.isDone)
        {
            _loadingBar.value = async.progress;
            break;
        }
    }

}
