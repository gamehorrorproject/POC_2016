﻿using UnityEngine;
using System.Collections;

public class PlaySound : IActionScript {

	public AudioClip sound;
    public float volume;
	public bool firstTime = true;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void Work()
	{
		if (firstTime == true) {
			GetComponent<AudioSource> ().PlayOneShot (sound, 0.1f);
			firstTime = false;
		}
	}
}
