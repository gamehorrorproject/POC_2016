using UnityEngine;
using System.Collections;

public class TurnOffAllLight : IActionScript {

    public AudioClip _sound;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void Work() {

		GameObject[] listOfLight;

		listOfLight = GameObject.FindGameObjectsWithTag ("Lamp");
        GetComponent<AudioSource>().PlayOneShot(_sound, 0.7f);

		foreach (GameObject light in listOfLight)
		{
			Light lightComponent;

			lightComponent = light.GetComponent<Light> ();
			//light.SetActive(false);
			if (lightComponent != null) {
				lightComponent.enabled = false;
			}
		}
	}
}
