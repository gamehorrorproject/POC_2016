﻿using UnityEngine;
using System.Collections;

public class ItemIsFalling : IActionScript {

	private bool playSong = false;
	public AudioClip chute;
	private float currentTime = 0.0f;
	private float totalTime = 2.0f;
	private bool firstTime = true;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (playSong == true) {
			currentTime += Time.deltaTime;
			if (currentTime >= totalTime) {
				GetComponent<AudioSource> ().PlayOneShot (chute, 0.7f);
				playSong = false;
			}
		}
	}
		
	public override void Work()
	{
		gameObject.SetActive (true);
		if (firstTime == true) {
			playSong = true;
			firstTime = false;
		}
	}
}
