﻿using UnityEngine;
using System.Collections;

public class DetectEvent : MonoBehaviour {

	bool finished = false;
	public IActionScript actionScript;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player")
		{
			actionScript.Work ();
			finished = true;
		}
	}
}
