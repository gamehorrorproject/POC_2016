
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DoorInteraction : MonoBehaviour
{
	public float smooth = 10.0f;
	public float DoorOpenAngle = 90.0f;
	public float DoorCloseAngle = 0.0f;
	//bool touchingDoor = false;
	public bool open = true;
    public AudioClip _sound;
	private GameObject message;


    private bool playedSong;

	void Start() {
		message = GameObject.Find("CardboardMain/Head/VrCanvas/Message");
		message.SetActive(false);
	}

	//Main function
    void Update()
    {
        if (tag != "LockedDoor")
        {
            //AudioClip sound = LoadAudio Resources.Load("DoorSound") as AudioClip;
            //GetComponent<AudioSource>() .PlayOneShot(_sound, 1.0f);
             
           
            if (open == true)
            {
                Quaternion target = Quaternion.Euler(0, DoorOpenAngle, 0);
                transform.localRotation = Quaternion.Slerp(transform.localRotation, target, Time.deltaTime * smooth);
            }

            else if (open == false)
            {
                Quaternion target1 = Quaternion.Euler(0, DoorCloseAngle, 0);
                transform.localRotation = Quaternion.Slerp(transform.localRotation, target1, Time.deltaTime * smooth);
            }
        }
        else
        {
            
            if (open == true)
            {
                GameObject key = GameObject.Find("CardboardMain/Head/VrCanvas/KeyInventory");
				if (key.activeInHierarchy)
                {
                    Quaternion target = Quaternion.Euler(0, DoorOpenAngle, 0);
                    transform.localRotation = Quaternion.Slerp(transform.localRotation, target, Time.deltaTime * smooth);
                }
                else
                {
					message = GameObject.Find("CardboardMain/Head/VrCanvas/Message");
					message.SetActive (true);
					open = false;
                    if (playedSong == true)
                        GetComponent<AudioSource>().PlayOneShot(_sound, 0.1f);
                    playedSong = false;
                }
            }

            else if (open == false)
            {
                Quaternion target1 = Quaternion.Euler(0, DoorCloseAngle, 0);
                transform.localRotation = Quaternion.Slerp(transform.localRotation, target1, Time.deltaTime * smooth);
            }
      }
 }

	public void InteractWithDoor()
	{
		bool tmp = !open;
        playedSong = true;
		open = tmp;
	}
}

