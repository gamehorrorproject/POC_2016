﻿using UnityEngine;
using System.Collections;

public class Corpse : IActionScript {

	public Vector3 startPosition;
	public Vector3 endPosition;
	public Vector3 pos;
	public AudioClip _sound;
	protected Animator anim;
	private float currentTime = 0f;
	private float totalTime = 5f;
	private bool isMoving = false;
	private float speed = 5f;
	private bool laugh = true;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		transform.position = startPosition;
	}

	public override void Work() {
		GameObject head = GameObject.FindGameObjectWithTag ("Head");
		GameObject light = GameObject.FindGameObjectWithTag ("Lamp");
		bool state = light.GetComponent<Light> ().isActiveAndEnabled;
		bool check = head.GetComponent<Light>().isActiveAndEnabled;
		if (check && !state && laugh)
		{
			transform.position = pos;
			GetComponent<AudioSource>().PlayOneShot(_sound, 0.7f);
			isMoving = true;
			laugh = false;
		}
	}
	// Update is called once per frame
	void Update () {
		if (isMoving) {
			currentTime += Time.deltaTime;
			transform.position = Vector3.Lerp(pos, endPosition, currentTime / totalTime);
			if (currentTime >= totalTime) {
				isMoving = false;
				currentTime = 0f;
				transform.position = startPosition;
			}
		}

	}

	public void goMove(Vector3 endpos) {
		startPosition = transform.position;
		endPosition = endpos;
		float dist = Vector3.Distance(startPosition, endPosition);
		totalTime = dist * 1f / speed;
		currentTime = 0f;
	}

	public bool IsMoving{
		get { return (isMoving);}
	}
}
