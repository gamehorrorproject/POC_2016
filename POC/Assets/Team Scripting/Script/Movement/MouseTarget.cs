﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MouseTarget : MonoBehaviour {

	private static MouseTarget mInstance;
	public static MouseTarget instance {get {return mInstance;}}

	public SpriteRenderer targetSprite;
	public GameObject reticle;

	// Use this for initialization
	void Start () {
		if (mInstance == null) {
			mInstance = this;
		}

		targetSprite.enabled = false;
	}

	public void OnFloor(bool op) {
		if (op) {
			targetSprite.enabled = true;
			reticle.SetActive(false);
		} else {
			targetSprite.enabled = false;
			reticle.SetActive(true);
		}
	}
}
