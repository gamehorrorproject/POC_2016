﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	private Vector3 startPosition;
	private Vector3 endPosition;
	private float currentTime = 0f;
	private float totalTime = 5f;
	private bool isMoving = false;
	private float speed = 5f;
	NavMeshAgent agent;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		/*if (isMoving) {
			/*currentTime += Time.deltaTime;
			transform.position = Vector3.Lerp(startPosition, endPosition, currentTime / totalTime);
			if (currentTime >= totalTime) {
				isMoving = false;
				currentTime = 0f;
				startPosition = transform.position;
			agent.GetComponent<NavMeshAgent> ();
			agent.SetDestination (endPosition);
			}
		}*/
	
	}

	public void goMove(Vector3 endpos) {
		/*startPosition = transform.position;
		endPosition = endpos;
		float dist = Vector3.Distance(startPosition, endPosition);
		totalTime = dist * 1f / speed;
		currentTime = 0f;*/
		endPosition = endpos;
		agent = gameObject.GetComponent<NavMeshAgent> ();
		agent.SetDestination (endPosition);

		isMoving = true;
	}

	public bool IsMoving{
		get { return (isMoving);}
	}
}
