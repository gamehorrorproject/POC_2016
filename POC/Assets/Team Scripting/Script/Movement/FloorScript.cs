using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class FloorScript : MonoBehaviour {
	
	public CardboardHead head;
	public GameObject player;

	public GameObject pointer;
	bool move = false;
	bool up = true;
	bool down = false;

	// Use this for initialization
	void Start () {
		pointer.SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
	{
		RaycastHit hit;
		Collider collider = GetComponent<Collider>();

		collider.Raycast (head.Gaze, out hit, Mathf.Infinity);
		pointer.transform.position = new Vector3 (hit.point.x, hit.point.y + 0.1f, hit.point.z);

		if (down && !up && move)
			{
				Vector3 obj = hit.point;
				obj = new Vector3(obj.x, player.transform.position.y, obj.z);
				// obj is the position pointed by the user
				player.GetComponent<PlayerController>().goMove(obj);
			}
	}

	public void OnEnter()
	{
		RaycastHit hit;
		Collider collider = GetComponent<Collider>();

		if (collider && head)
		{
			collider.Raycast (head.Gaze, out hit, Mathf.Infinity);

			if (MouseTarget.instance != null)
			{
				MouseTarget.instance.OnFloor (true);
				pointer.transform.position = new Vector3 (hit.point.x, hit.point.y + 0.1f, hit.point.z);
				
				pointer.SetActive (true);
			}
		}

		move = true;
	}

	public void OnExit()
	{
		RaycastHit hit;
		Collider collider = GetComponent<Collider>();

		if (collider && head)
		{
			collider.Raycast (head.Gaze, out hit, Mathf.Infinity);

			if (MouseTarget.instance != null)
			{
				MouseTarget.instance.OnFloor (false);

				pointer.SetActive (false);
			}
		}

		move = false;
	}

	public void ButtonUp()
	{
		up = true;
		down = false;
	}

	public void ButtonDown()
	{
		down = true;
		up = false;
	}

	void LateUpdate() {
		Cardboard.SDK.UpdateState();
		if (Cardboard.SDK.BackButtonPressed) {
			Application.Quit();
		}
	}
}
