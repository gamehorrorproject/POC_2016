﻿// Copyright 2015 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class DebugText : MonoBehaviour {
  private Text text;

  void Awake() {
    text = GetComponent<Text>();
  }

  void LateUpdate() {
		//Quaternion rotation = Cardboard.SDK.HeadPose.Orientation;
		//text.text = "x: " + rotation.eulerAngles.x;
  }

	public void setText(string text) {
		this.text.text = text;
	}

}
