﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InventoryScript : MonoBehaviour {

    //private Vector3 initPos = new Vector3(0, 0, 0);
    private Vector3 separation = new Vector3(0, 0, 0);

	private List<ObjectModel> _list;
    public GameObject _key;

	// Use this for initialization
	void Start () {
        //_key = GameObject.Find("CardboardMain/Head/VrCanvas/KeyInventory");
        _key.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

	}

    public void tryElement() {

    }

	public void addObject(ObjectModel obj) {
        switch (obj.Name) {
            case "Key":

                if (_list == null)
                    _list = new List<ObjectModel>();
                _key = GameObject.Find("CardboardMain/Head/VrCanvas/KeyInventory");
				GameObject message = GameObject.Find("CardboardMain/Head/VrCanvas/Message");
				message.SetActive (false);
                //_key.transform.position += separation;
                _key.SetActive(true);
                obj.Objet = _key;
                _list.Add(obj);
                break;
			case "HandLight":

                if (_list == null)
                    _list = new List<ObjectModel>();
                _key = GameObject.Find("CardboardMain/Head/VrCanvas/FlashLightInventory");
                _key.SetActive(true);
                obj.Objet = _key;
                _list.Add(obj);
                break;
            default:
                break;
        }
     }

	// IL faut gérer le GUI dans CETTE CLASSE (Affiche la clef ou autre par exemple)
	// L'image de la clef est en dur. Il faudrait créer un UI Image et set sa Ressource (celle présente dans OBject Model)
}
