﻿using UnityEngine;
using System.Collections;

public class GetObject : MonoBehaviour
{
	public CardboardHead head;
	public float maxRange;
	public Canvas inventoryGUI;
    private InventoryScript _inventoryScript;

	// Use this for initialization
	void Start () {
        _inventoryScript = new InventoryScript();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnEnter ()
	{
		RaycastHit hit;
		Collider collider = GetComponent<Collider>();
		bool isLookedAt = false;

		if (collider && head)
			isLookedAt = collider.Raycast (head.Gaze, out hit, maxRange);

		if (isLookedAt)
		GetComponent<Renderer>().material.color = Color.red;
	}

	public void OnExit ()
	{
		GetComponent<Renderer>().material.color = Color.white;
	}

	public void OnClick()
	{
		Debug.Log ("OnClick");
		RaycastHit hit;
		Collider collider = GetComponent<Collider>();
		bool isLookedAt = false;

		if (collider && head)
			isLookedAt = collider.Raycast (head.Gaze, out hit, maxRange);

		if (isLookedAt) {

            ObjectModel objectmodel = new ObjectModel();

            objectmodel.Name = gameObject.name;
            _inventoryScript.addObject(objectmodel);

			if (gameObject.tag == "CollectLamp") {
					head.GetComponent<Light> ().enabled = true;
			}
			Destroy (gameObject);
		}
	}
}
