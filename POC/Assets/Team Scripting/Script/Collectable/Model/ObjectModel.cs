﻿using UnityEngine;
using System.Collections;

public class ObjectModel {

	static int all_id = 0;
	int _id;

	string _name;
	string resources_path;
    GameObject objet;

	public Sprite sprite = null;

	public ObjectModel() {
		_id = all_id;
		all_id++;
	}

	public int Id {
		get {
			return this._id;
		}
		set {
			_id = value;
		}
	}

	public string Name {
		get {
			return this._name;
		}
		set {
			_name = value;
		}
	}
		
	public string Resources_path {
		get {
			return this.resources_path;
		}
		set {
			resources_path = value;
			sprite = Resources.Load(resources_path) as Sprite;
		}
	}

    public GameObject Objet
    {
        get
        {
            return (this.objet);
        }
        set
        {
            objet = value;
        }
    }

}
