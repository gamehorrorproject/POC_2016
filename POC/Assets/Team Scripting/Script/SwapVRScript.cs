﻿using UnityEngine;
using System.Collections;

public class SwapVRScript : MonoBehaviour {
		
		public GameObject[] cardboardObjects;
		public GameObject[] monoObjects;
		
		// Turn on or off VR mode
		void ActivateVRMode(bool goToVR)
		{
			GameObject eventSystem;
			MonoBehaviour script;

			foreach (GameObject cardboardThing in cardboardObjects) {
				cardboardThing.SetActive(goToVR);
			}
			foreach (GameObject monoThing in monoObjects) {
				monoThing.SetActive(!goToVR);
			}

			Cardboard.SDK.VRModeEnabled = goToVR;

			eventSystem = GameObject.Find("EventSystem");
			script = eventSystem.GetComponent<GazeInputModule> ();
			script.enabled = goToVR;

			// Tell the game over screen to redisplay itself if necessary
			gameObject.GetComponent<GameController>().Refresh();
		}
		
		public void Switch() {
			ActivateVRMode(!Cardboard.SDK.VRModeEnabled);
		}
		
		void Update () {
		if (Cardboard.SDK.VRModeEnabled)
			if (Cardboard.SDK.BackButtonPressed) {
				Switch();
			}
		}
		
		void Start() {
			ActivateVRMode(false);
		}
}