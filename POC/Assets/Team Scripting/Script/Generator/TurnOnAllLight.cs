using UnityEngine;
using System.Collections;

public class TurnOnAllLight : MonoBehaviour {

	public CardboardHead head;
	public float maxRange;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClick()
	{
		GameObject[] listOfLight;
		RaycastHit hit;
		Collider collider = GetComponent<Collider>();
		bool isLookedAt = false;

		if (collider && head) {
			isLookedAt = collider.Raycast (head.Gaze, out hit, maxRange);
		}
		if (isLookedAt) {
			listOfLight = GameObject.FindGameObjectsWithTag ("Lamp");
			Debug.Log ("Generator");
			foreach (GameObject light in listOfLight) {
				Light lightComponent;

				lightComponent = light.GetComponent<Light> ();
				//light.SetActive(false);
				if (lightComponent != null) {
					Debug.Log("LAMP");
					lightComponent.enabled = true;
				}
			}
		}
	}

}
